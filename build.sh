declare -a repos=("Simple-App-Launcher" "Simple-Calculator" "Simple-Calendar" "Simple-Camera" "Simple-Clock" "Simple-Contacts" "Simple-Dialer" "Simple-Draw" "Simple-File-Manager" "Simple-Flashlight" "Simple-Gallery" "Simple-Keyboard" "Simple-Music-Player" "Simple-Notes" "Simple-SMS-Messenger" "Simple-Thank-You" "Simple-Voice-Recorder")

for repo in ${repos[@]}; do
    description=$(gh repo view $(echo SimpleMobileTools/$repo | sed 's|:.*||') | grep description | sed 's/.*\t//')
    release=$(gh release list --repo SimpleMobileTools/$repo | grep Latest | sed 's/.*Latest\t//' | sed 's/\t.*//')
    changelog="$(gh release view --repo SimpleMobileTools/$repo $release | sed -z 's/ASSETS.*//' | sed '1,10d' | sed '/^[[:space:]]*$/d')"

    NO_PROMPT=1 glab repo create julianfairfax/$repo --public --description "$description"

    glab variable set KEYSTORE "$KEYSTORE" --repo julianfairfax/$repo

    glab variable set KEYSTORE_PROPERTIES "$KEYSTORE_PROPERTIES" --repo julianfairfax/$repo

    glab variable set GL_ACCESS_TOKEN "$GL_ACCESS_TOKEN" --repo julianfairfax/$repo

    branch=$release

    git clone https://gitlab.com/julianfairfax/$repo

    cd $repo

    git_branch=$(git describe --abbrev=0 --tags)

    cd ../

    rm -rf $repo

    if [[ ! $branch == $git_branch ]]; then        
        if [[ ! -d Simple-Commons ]]; then
            NO_PROMPT=1 glab repo create julianfairfax/Simple-Commons --public --description "$(gh repo view $(echo SimpleMobileTools/Simple-Commons | sed 's|:.*||') | grep description | sed 's/.*\t//')"

            git clone https://github.com/SimpleMobileTools/Simple-Commons
        fi
        
        git clone https://github.com/SimpleMobileTools/$repo

        cd $repo

        if [[ $repo == Simple-Camera ]]; then
            git checkout -b $branch
        else
            git fetch --all --tags
        
            git checkout tags/$branch -b $branch
        fi

        branch=$(cat app/build.gradle | grep Simple-Commons | sed "s/    implementation 'com.github.SimpleMobileTools:Simple-Commons://" | sed "s/'//")
        
        cd ../Simple-Commons

        git clone https://gitlab.com/julianfairfax/Simple-Commons

        cd Simple-Commons

        git_branch=$(git branch -a | grep $branch | head -n 1 | sed 's|.*/||' | sed 's/.*\ //')

        if [[ ! $git_branch == $branch ]]; then
            cd ../

            rm -rf Simple-Commons

            git checkout $branch -b $branch

            sed -i "s/18 -> getColors(R.array.md_greys)/18 -> getColors(R.array.md_greys)\n        19 -> getColors(R.array.md_greys)/" commons/src/main/kotlin/com/simplemobiletools/commons/dialogs/LineColorPickerDialog.kt

            sed -i "s/\".Grey_black\"/\".Grey_black\",\n    \".Grey_white\"/" commons/src/main/kotlin/com/simplemobiletools/commons/helpers/Constants.kt

            sed -i "0,/<item>@color\/md_grey_black<\/item>/s//<item>@color\/md_grey_black<\/item>\n        <item>@color\/md_grey_white<\/item>/" commons/src/main/res/values/arrays.xml

            git remote set-url origin https://gitlab.com/julianfairfax/Simple-Commons

            git config --global user.name 'julianfairfax'

            git config --global user.email 'juliannfairfax@protonmail.com'

            git add .

            git commit -m "Automated update"

            git push https://julianfairfax:$GL_ACCESS_TOKEN@gitlab.com/julianfairfax/Simple-Commons.git

            export commit=$(git log -n 1 --pretty=format:"%H" | cut -c 1-10)
        else
            git checkout remotes/origin/$branch

            export commit=$(git log -n 1 --pretty=format:"%H" | cut -c 1-10)

            cd ../

            rm -rf Simple-Commons
        fi

        cd ../

        cd $repo

        sed -i "s/implementation 'com.github.SimpleMobileTools:Simple-Commons:.*'/implementation 'com.gitlab.julianfairfax:Simple-Commons:$commit'/" app/build.gradle

        sed -i '0,/android:icon="@mipmap\/ic_launcher"/s//android:icon="@mipmap\/ic_launcher_grey_white"/' app/src/main/AndroidManifest.xml

        sed -i '0,/android:roundIcon="@mipmap\/ic_launcher"/s//android:roundIcon="@mipmap\/ic_launcher_grey_white"/' app/src/main/AndroidManifest.xml

        sed -i -z "s/<activity-alias\n            android:name=\".activities.SplashActivity.Grey_black\"\n            android:enabled=\"false\"\n            android:exported=\"true\"\n            android:icon=\"@mipmap\/ic_launcher_grey_black\"\n            android:roundIcon=\"@mipmap\/ic_launcher_grey_black\"\n            android:targetActivity=\".activities.SplashActivity\">\n\n            <intent-filter>\n                <action android:name=\"android.intent.action.MAIN\" \/>\n                <category android:name=\"android.intent.category.LAUNCHER\" \/>\n            <\/intent-filter>\n        <\/activity-alias>/<activity-alias\n            android:name=\".activities.SplashActivity.Grey_black\"\n            android:enabled=\"false\"\n            android:exported=\"true\"\n            android:icon=\"@mipmap\/ic_launcher_grey_black\"\n            android:roundIcon=\"@mipmap\/ic_launcher_grey_black\"\n            android:targetActivity=\".activities.SplashActivity\">\n\n            <intent-filter>\n                <action android:name=\"android.intent.action.MAIN\" \/>\n                <category android:name=\"android.intent.category.LAUNCHER\" \/>\n            <\/intent-filter>\n        <\/activity-alias>\n\n        <activity-alias\n            android:name=\".activities.SplashActivity.Grey_white\"\n            android:enabled=\"false\"\n            android:exported=\"true\"\n            android:icon=\"@mipmap\/ic_launcher_grey_white\"\n            android:roundIcon=\"@mipmap\/ic_launcher_grey_white\"\n            android:targetActivity=\".activities.SplashActivity\">\n\n            <intent-filter>\n                <action android:name=\"android.intent.action.MAIN\" \/>\n                <category android:name=\"android.intent.category.LAUNCHER\" \/>\n            <\/intent-filter>\n        <\/activity-alias>/" app/src/main/AndroidManifest.xml

        if [[ "$(cat "$(find -name SimpleActivity.kt)" | grep "            R.mipmap.ic_launcher_grey_black")" == "            R.mipmap.ic_launcher_grey_black" ]]; then
            sed -i "s/            R.mipmap.ic_launcher_grey_black/            R.mipmap.ic_launcher_grey_black,\n            R.mipmap.ic_launcher_grey_white/" "$(find -name SimpleActivity.kt)"
        elif [[ "$(cat "$(find -name SimpleActivity.kt)" | grep "        R.mipmap.ic_launcher_grey_black")" == "        R.mipmap.ic_launcher_grey_black" ]]; then
            sed -i "s/        R.mipmap.ic_launcher_grey_black/        R.mipmap.ic_launcher_grey_black,\n        R.mipmap.ic_launcher_grey_white/" "$(find -name SimpleActivity.kt)"
        fi

        cp app/src/main/res/mipmap-anydpi-v26/ic_launcher_grey_black.xml app/src/main/res/mipmap-anydpi-v26/ic_launcher_grey_white.xml

        sed -i 's/grey_black/grey_white/' app/src/main/res/mipmap-anydpi-v26/ic_launcher_grey_white.xml

        sed -i 's/ic_launcher_foreground/ic_launcher_foreground_white/' app/src/main/res/mipmap-anydpi-v26/ic_launcher_grey_white.xml

        for mipmap in app/src/main/res/*; do
            if [[ $mipmap == *mipmap* && ! $mipmap == *anydpi-v26 ]]; then
                convert $mipmap/ic_launcher_foreground.png -channel RGB -negate $mipmap/ic_launcher_foreground_white.png

                convert $mipmap/ic_launcher_grey_black.png -channel RGB -negate $mipmap/ic_launcher_grey_white.png
            fi
        done

        sed -i -z "s/\n        proprietary {}//" app/build.gradle

        echo "build:
  image: debian:latest
  stage: build
  script:
    - apt update
    - apt install openjdk-11-jdk git curl gpg android-sdk -y
    - curl -q 'https://proget.makedeb.org/debian-feeds/prebuilt-mpr.pub' | gpg --dearmor | tee /usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg 1> /dev/null
    - echo \"deb [signed-by=/usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg] https://proget.makedeb.org prebuilt-mpr bullseye\" | tee /etc/apt/sources.list.d/prebuilt-mpr.list
    - apt update
    - apt install glab -y
    - echo \"deb http://deb.debian.org/debian unstable main\" | tee /etc/apt/sources.list
    - apt update
    - apt install -t unstable sdkmanager -y
    - glab auth login --token $GL_ACCESS_TOKEN
    - glab config set -h gitlab.com git_protocol https
    - glab config set -h gitlab.com api_protocol https
    - bash build.sh
    - if [[ -d app/build/outputs/apk/fdroid/release ]]; then path=\$(ls -1 app/build/outputs/apk/fdroid/release/*-fdroid-release.apk); elif [[ -d app/build/outputs/apk/foss/release ]]; then path=\$(ls -1 app/build/outputs/apk/foss/release/*-foss-release.apk); fi    
    - if [[ -d app/build/outputs/apk/fdroid/release ]]; then filename=\$(basename \$(ls -1 app/build/outputs/apk/fdroid/release/*-fdroid-release.apk) ); elif [[ -d app/build/outputs/apk/foss/release ]]; then filename=\$(basename \$(ls -1 app/build/outputs/apk/foss/release/*-foss-release.apk) ); fi
    - export path="\$path"
    - export filename="\$filename"
    - bash release.sh" | tee .gitlab-ci.yml

        echo "if [[ \$(stat -c %s \$path) -lt 10000000 ]]; then
    echo \"glab release create \"$(git symbolic-ref --short HEAD | sed 's|heads/||')\" --notes \\\"$changelog\\\" '\"\$path\"#\"\$filename\"'\" | sed 's/        //' | bash
else
    split --bytes=10MB \$path \$path

    echo \"glab release create \"$(git symbolic-ref --short HEAD | sed 's|heads/||')\" --notes \\\"$changelog\\\" '\"\$(echo \$path)aa\"#\"\$(echo \$filename)aa\"'\" | sed 's/        //' | bash

    echo \"glab release create \"$(git symbolic-ref --short HEAD | sed 's|heads/||')\" --notes \\\"$changelog\\\" '\"\$(echo \$path)ab\"#\"\$(echo \$filename)ab\"'\" | sed 's/        //' | bash

    if [[ -f \$(echo \$path)ac ]]; then
        echo \"glab release create \"$(git symbolic-ref --short HEAD | sed 's|heads/||')\" --notes \\\"$changelog\\\" '\"\$(echo \$path)ac\"#\"\$(echo \$filename)ac\"'\" | sed 's/        //' | bash
    fi
fi" | tee release.sh

        echo "echo \"\$KEYSTORE_PROPERTIES\" | base64 -d - > keystore.properties

echo \"\$KEYSTORE\" | base64 -d - > keystore.keystore

yes | ANDROID_HOME=\"/usr/lib/android-sdk\" sdkmanager --licenses

ANDROID_HOME=\"/usr/lib/android-sdk\" ./gradlew assembleRelease

while [[ ! -d app/build/outputs/apk/fdroid/release && ! -d app/build/outputs/apk/foss/release ]]; do
    ANDROID_HOME=\"/usr/lib/android-sdk\" ./gradlew assembleRelease
done" | tee build.sh

        git remote set-url origin https://gitlab.com/julianfairfax/$repo

        git config --global user.name 'julianfairfax'

        git config --global user.email 'juliannfairfax@protonmail.com'

        git add .

        git commit -m "Automated update"

        git push https://julianfairfax:$GL_ACCESS_TOKEN@gitlab.com/julianfairfax/$repo.git --tags

        git push https://julianfairfax:$GL_ACCESS_TOKEN@gitlab.com/julianfairfax/$repo.git
        
        cd ../

        rm -rf $repo
    fi
done

rm -rf Simple-Commons

built=0

while [[ $built == *0* ]]; do
    built=""

    for repo in ${repos[@]}; do
        release=$(gh release list --repo SimpleMobileTools/$repo | grep Latest | sed 's/.*Latest\t//' | sed 's/\t.*//')
        
        if [[ ! $release == $(glab release list --repo julianfairfax/$repo | grep $release | sed 's/\t.*//') ]]; then
            built="$built 0"
        fi
    done

    sleep 10s
done