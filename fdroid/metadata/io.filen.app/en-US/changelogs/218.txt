- Fixed a bug where the app would immediately crash when opening it
- Upload speed increased
- General bug fixes and performance improvements
