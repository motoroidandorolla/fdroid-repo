Notable changes in version 14:

* add initial support for password protected encrypted PDFs
* add support for saving current PDF ("Save as" entry in the menu)
* move share action lower in the menu
* remove obsolete zoom in / zoom out menu entries
* simplify message for errors encountered opening PDFs
* destroy WebView when activity is destroyed to release resources
* extend Permissions Policy by disabling new standard feature flags
* update Gradle to 7.4.2
* update Android Gradle plugin to 7.1.3
* update Kotlin Gradle plugin to 1.6.21
* replace some of the deprecated Android APIs

A full list of changes from the previous release (version 13) is available through the [Git commit log between the releases](https://github.com/GrapheneOS/PdfViewer/compare/13...14).

----

Simple Android PDF viewer based on pdf.js and content providers. The app doesn't require any permissions. The PDF stream is fed into the sandboxed WebView without giving it access to content or files. Content-Security-Policy is used to enforce that the JavaScript and styling properties within the WebView are entirely static content from the apk assets. It reuses the hardened Chromium rendering stack while only exposing a tiny subset of the attack surface compared to actual web content. The PDF rendering code itself is memory safe with dynamic code evaluation disabled, and even if an attacker did gain code execution by exploiting the underlying web rendering engine, they're within the Chromium renderer sandbox with no access to the network (unlike a browser), files, or other content.

----

This app is [available through the Play Store with the `app.grapheneos.pdfviewer.play` app id](https://play.google.com/store/apps/details?id=app.grapheneos.pdfviewer.play). Play Store releases go through review and it usually takes around 1 to 3 days before the Play Store pushes out the update to users. Play Store releases use Play Signing, so we use a separate app id from the releases we publish ourselves to avoid conflicts and to distinguish between them.

Releases of the app signed by GrapheneOS with the `app.grapheneos.pdfviewer` app id are published in the GrapheneOS app repository and on GitHub. You can use the [GrapheneOS app repository client](https://github.com/GrapheneOS/Apps/releases) on Android 12 or later for automatic updates.

Releases are initially pushed out through the Beta channel for both the Play Store and our app repository and then get moved to the Stable channel.
