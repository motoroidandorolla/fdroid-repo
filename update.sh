declare -a gitlab_repos=("julianfairfax/Simple-App-Launcher:System" "julianfairfax/Simple-Calculator:System" "julianfairfax/Simple-Calendar:System" "julianfairfax/Simple-Camera:System" "julianfairfax/Simple-Clock:System" "julianfairfax/Simple-Contacts:System" "julianfairfax/Simple-Dialer:System" "julianfairfax/Simple-Draw:System" "julianfairfax/Simple-File-Manager:System" "julianfairfax/Simple-Flashlight:System" "julianfairfax/Simple-Gallery:System" "julianfairfax/Simple-Keyboard:System" "julianfairfax/Simple-Music-Player:System" "julianfairfax/Simple-Notes:System" "julianfairfax/Simple-SMS-Messenger:System" "julianfairfax/Simple-Thank-You:System" "julianfairfax/Simple-Voice-Recorder:System")
declare -a github_repos=("FilenCloudDienste/filen-mobile:Internet" "GrapheneOS/Apps:System" "GrapheneOS/Auditor:System" "GrapheneOS/Camera:System" "GrapheneOS/PdfViewer:System" "Darkempire78/OpenCalc:System" "ProtonMail/proton-mail-android:Internet")

if [[ ! -d fdroid/metadata ]]; then
    mkdir -p fdroid/metadata
fi

if [[ ! -d fdroid/repo ]]; then
    mkdir -p fdroid/repo
fi

rm fdroid/repo/*.apk

for gitlab_repo in ${gitlab_repos[@]}; do
    repo=$(echo $gitlab_repo | sed 's|:.*||')
    release=$(glab release list --repo $repo | sed -n '3p' | sed 's/\t.*//')
    asset=$(glab release view --repo $repo $release | grep https://gitlab.com/$repo/-/releases/$release/downloads/.*apk | sed 's/\t.*//')
    description=$(glab repo view $repo | grep description | sed 's/.*\t//')
    changelog="$(glab release view --repo $repo $release | sed -z 's/ASSETS.*//' | sed '1,3d' | sed '/^[[:space:]]*$/d' | sed 's/\x1B\[[0-9;]*[JKmsu]//g')"

    if [[ $(echo "$asset" | wc -l) == 1 ]]; then           
        glab release download $release --repo $repo --asset-name="$asset" --dir fdroid/repo
    else
        glab release download $release --repo $repo --asset-name="$(echo "$asset" | grep .apkaa)" --dir fdroid/repo
        
        glab release download $release --repo $repo --asset-name="$(echo "$asset" | grep .apkab)" --dir fdroid/repo

        if [[ $(echo "$asset" | wc -l) == 3 ]]; then 
            glab release download $release --repo $repo --asset-name="$(echo "$asset" | grep .apkac)" --dir fdroid/repo

            cat fdroid/repo/$(echo "$asset" | grep .apkaa) fdroid/repo/$(echo "$asset" | grep .apkab) fdroid/repo/$(echo "$asset" | grep .apkac) > fdroid/repo/$(echo "$asset" | grep .apkab | sed 's/.apkab/.apk/')

            rm fdroid/repo/$(echo "$asset" | grep .apkaa)
            
            rm fdroid/repo/$(echo "$asset" | grep .apkab)

            rm fdroid/repo/$(echo "$asset" | grep .apkac)
        else
            cat fdroid/repo/$(echo "$asset" | grep .apkaa) fdroid/repo/$(echo "$asset" | grep .apkab) > fdroid/repo/$(echo "$asset" | grep .apkab | sed 's/.apkab/.apk/')

            rm fdroid/repo/$(echo "$asset" | grep .apkaa)
            
            rm fdroid/repo/$(echo "$asset" | grep .apkab)
        fi
    fi

    name=$(aapt dump badging fdroid/repo/$(echo "$asset" | grep -v apkab | grep -v apkac | sed 's/apkaa/apk/') | grep application-label: | sed "s/application-label:'//" | sed "s/'.*//")
    version=$(aapt dump badging fdroid/repo/$(echo "$asset" | grep -v apkab | grep -v apkac | sed 's/apkaa/apk/') | grep versionCode | sed "s/.*versionCode='//" | sed "s/'.*//")
    id=$(aapt dump badging fdroid/repo/$(echo "$asset" | grep -v apkab | grep -v apkac | sed 's/apkaa/apk/') | grep package:\ name | sed "s/package: name='//" | sed "s/'.*//")

    if [[ -z "$description" ]]; then
        description="$name"
    else
        description="$description"
    fi

    echo "AuthorName: $(echo $gitlab_repo | sed 's|/.*||')
Categories:
    - $(echo $gitlab_repo | sed 's|.*:||')
CurrentVersion: \"$release\"
CurrentVersionCode: $version
Description: |
    $description
IssueTracker: https://gitlab.com/$repo/-/issues
License: \"\"
Name: $name
SourceCode: https://gitlab.com/$repo
Summary: \"$(echo $description | cut -c 1-80)\"
WebSite: \"\"" | tee fdroid/metadata/$id.yml

    if [[ -d fdroid/metadata/$id/en-US/changelogs ]]; then
        rm -r fdroid/metadata/$id/en-US/changelogs
    fi

    mkdir -p fdroid/metadata/$id/en-US/changelogs

    echo "$changelog" | tee fdroid/metadata/$id/en-US/changelogs/$version.txt
done

for github_repo in ${github_repos[@]}; do   
    repo=$(echo $github_repo | sed 's|:.*||')
    release=$(gh release list --repo $repo | grep Latest | sed 's/.*Latest\t//' | sed 's/\t.*//')
    asset=$(gh release view $release --repo $repo | grep asset: | grep ".*apk" | sed 's/asset:\t//')
    description=$(gh repo view $repo | grep description: | sed 's/.*\t//')
    changelog="$(gh release view --repo $repo $release | sed '1,10d')"

    gh release download $release --repo $repo --pattern "*.apk" --dir fdroid/repo

    name=$(aapt dump badging fdroid/repo/$asset | grep application-label: | sed "s/application-label:'//" | sed "s/'.*//")
    version=$(aapt dump badging fdroid/repo/$asset | grep versionCode | sed "s/.*versionCode='//" | sed "s/'.*//")
    id=$(aapt dump badging fdroid/repo/$asset | grep package:\ name | sed "s/package: name='//" | sed "s/'.*//")

    if [[ -z "$description" ]]; then
        description="$name"
    else
        description="$description"
    fi

    echo "AuthorName: $(echo $github_repo | sed 's|/.*||')
Categories:
    - $(echo $github_repo | sed 's|.*:||')
CurrentVersion: \"$release\"
CurrentVersionCode: $version
Description: |
    $description
IssueTracker: https://github.com/$repo/issues
License: \"\"
Name: $name
SourceCode: https://github.com/$repo
Summary: \"$(echo $description | cut -c 1-80)\"
WebSite: \"\"" | tee fdroid/metadata/$id.yml

    if [[ -d fdroid/metadata/$id/en-US/changelogs ]]; then
        rm -r fdroid/metadata/$id/en-US/changelogs
    fi

    mkdir -p fdroid/metadata/$id/en-US/changelogs

    echo "$changelog" | tee fdroid/metadata/$id/en-US/changelogs/$version.txt
done

if [[ ! -f fdroid/repo/index-v1.json ]]; then
    echo '{"repo": {"timestamp": 1654932141000, "version": 20001, "name": "Efreak Repo", "icon": "icon.png", "address": "https://raw.githubusercontent.com/efreak/auto-daily-fdroid/main/fdroid/repo", "description": "This is a repository of apps to be used with F-Droid. Applications in this repository are official binaries built by the original application developers and released on the official github pages.\nIncluded apps are FastHub (not listed in fdroid), FastHub-Re (also not listed, a more up-to-date fork of FastHub-Libre) and Hacki (updates are too fast for fdroid). More might be added eventually."}, "requests": {"install": [], "uninstall": []}, "apps": [], "packages": {}}' | tee fdroid/repo/index-v1.json
fi

cd fdroid

/usr/bin/fdroid update --pretty --delete-unknown

cd ../

git config --global user.name 'julianfairfax'
git config --global user.email 'juliannfairfax@protonmail.com'
git config --global http.postBuffer 524288000

git add .
git commit -m"Automated update"
git push https://julianfairfax:$GL_ACCESS_TOKEN@gitlab.com/julianfairfax/fdroid-repo.git HEAD:main