if [[ ! -f "${0%/*}"/gitlab.txt || ! -f "${0%/*}"/github.txt ]]; then
    read -e -p "[rebuild.sh] GL_ACCESS_TOKEN: " GL_ACCESS_TOKEN
    read -e -p "[rebuild.sh] GH_ACCESS_TOKEN: " GH_ACCESS_TOKEN
fi

if [[ -f "${0%/*}"/gitlab.txt ]]; then
    GL_ACCESS_TOKEN=$(cat "${0%/*}"/gitlab.txt)
fi

if [[ -f "${0%/*}"/github.txt ]]; then
    GH_ACCESS_TOKEN=$(cat "${0%/*}"/github.txt)
fi

declare -a repos=("fdroid-repo" "Simple-Commons" "Simple-App-Launcher" "Simple-Calculator" "Simple-Calendar" "Simple-Camera" "Simple-Clock" "Simple-Contacts" "Simple-Dialer" "Simple-Draw" "Simple-File-Manager" "Simple-Flashlight" "Simple-Gallery" "Simple-Keyboard" "Simple-Music-Player" "Simple-Notes" "Simple-SMS-Messenger" "Simple-Thank-You" "Simple-Voice-Recorder")

for repo in ${repos[@]}; do
    glab repo delete $repo --yes

    if [[ $repo == fdroid-repo ]]; then
        sleep 10s
        
        NO_PROMPT=1 glab repo create $repo --public --description "Repository for installing apps more easily"

        base64 "${0%/*}"/fdroid/config.yml | glab variable set CONFIG_YML --repo julianfairfax/fdroid-repo

        base64 "${0%/*}"/fdroid/keystore.p12 | glab variable set KEYSTORE_P12 --repo julianfairfax/fdroid-repo

        base64 "${0%/*}"/keystore.keystore | glab variable set KEYSTORE --repo julianfairfax/$repo

        base64 "${0%/*}"/keystore.properties | glab variable set KEYSTORE_PROPERTIES --repo julianfairfax/$repo

        echo "$GL_ACCESS_TOKEN" | glab variable set GL_ACCESS_TOKEN --repo julianfairfax/$repo

        echo "$GH_ACCESS_TOKEN" | glab variable set GH_ACCESS_TOKEN --repo julianfairfax/$repo
    fi
done

cd "${0%/*}"

rm -rf .git

rm -rf fdroid/archive

rm -rf fdroid/metadata

rm -rf fdroid/repo

git init

git remote add origin https://gitlab.com/julianfairfax/fdroid-repo

git add .

git commit -m "Update"

git push --set-upstream origin main