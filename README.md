# F-Droid Repo
Repository for installing apps more easily.

## Updates
This repository is updated every day at 00:00 UTC+2. If updates are available at this time, you will receive them after the building process is completed.

## Apps
The following apps made by others are available from this repo:
- [GrapheneOS Apps](https://github.com/GrapheneOS/Apps)
- [GrapheneOS Auditor](https://github.com/GrapheneOS/Auditor)
- [GrapheneOS Camera](https://github.com/GrapheneOS/Camera)
- [GrapheneOS PDF Viewer](https://github.com/GrapheneOS/PdfViewer)
- [Filen](https://github.com/FilenCloudDienste/filen-mobile)
- [OpenCalc](https://github.com/Darkempire78/OpenCalc)
- [Proton Mail](https://github.com/ProtonMail/proton-mail-android)

In addition, the following apps made by others, and with patches made by me, are available from this repo:
- [Simple Mobile Tools](https://github.com/SimpleMobileTools), with [patches](/build.sh#L64-L125) to support a white icon. I do not make any claim to the source code, and believe these modifications comply with the GPL-3.0 license used by the project, as the changes are disclosed, open source, and under the same license.
  - [Simple App Launcher](https://github.com/SimpleMobileTools/Simple-App-Launcher)
  - [Simple Calculator](https://github.com/SimpleMobileTools/Simple-Calculator)
  - [Simple Calendar](https://github.com/SimpleMobileTools/Simple-Calendar)
  - [Simple Camera](https://github.com/SimpleMobileTools/Simple-Camera)
  - [Simple Clock](https://github.com/SimpleMobileTools/Simple-Clock)
  - [Simple Contacts](https://github.com/SimpleMobileTools/Simple-Contacts)
  - [Simple Dialer](https://github.com/SimpleMobileTools/Simple-Dialer)
  - [Simple Draw](https://github.com/SimpleMobileTools/Simple-Draw)
  - [Simple File Manager](https://github.com/SimpleMobileTools/Simple-File-Manager)
  - [Simple Flashlight](https://github.com/SimpleMobileTools/Simple-Flashlight)
  - [Simple Gallery](https://github.com/SimpleMobileTools/Simple-Gallery)
  - [Simple Keyboard](https://github.com/SimpleMobileTools/Simple-Keyboard)
  - [Simple Music Player](https://github.com/SimpleMobileToolsSimple-Music-Player)
  - [Simple Notes](https://github.com/SimpleMobileTools/Simple-Notes)
  - [Simple SMS Messenger](https://github.com/SimpleMobileTools/Simple-SMS-Messenger)
  - [Simple Thank You](https://github.com/SimpleMobileTools/Simple-Thank-You)
  - [Simple Voice Recorder](https://github.com/SimpleMobileTools/Simple-Voice-Recorder)

## How to use
Copy this link and add it to your F-Droid client:

```
https://gitlab.com/julianfairfax/fdroid-repo/-/raw/main/fdroid/repo?fingerprint=561AD800D900CA8EBF3A531C6A860B5AB8569D0D376FD121CBCCFCA9434D7DF7
```

You can also scan this QR code and add the link to your F-Droid client:

![F-Droid repo QR code](https://gitlab.com/julianfairfax/fdroid-repo/-/raw/main/fdroid/repo/icons/icon.png)

## License
The apps hosted in the fdroid folder of repository are under the licenses of their developers. This repository's license only applies to the code for the repository itself.

If you believe your code has been unjustify used or has been used without proper credit in this repository, please [contact](https://julianfairfax.gitlab.io/contact.html) me.

### Self-hosting information
If you are going to host a repository like this yourself, it is requested that you remove my content from it. This can be done by removing the fdroid folder.

If you wish to host a repository like this on GitHub instead of GitLab, you should check out [this repo](https://github.com/Efreak/seeker-fdroid) instead, as this one is not made to be compatible.